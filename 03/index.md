# Part III: Contract Origination and Interaction

In the last two chapters, we learned how to write some simple Michelson programs
and how to use the Tezos command-line tools to interact with the Tezos
blockchain.  In this chapter we will combine both these topics in order to write
a simple Michelson contract, originate it onto the alphanet chain, and then
interact with it in various ways. This will prepare us for the following
chapters, where we will increase the complexity and sophistication of the
Michelson programs we can write.

## Contract origination

To start, let's push a simple contract onto the Alphanet.

We'll use the following file `idString.tz`:

```
parameter string;
storage string;
code {CAR; NIL operation; PAIR;};
```

This simple contract is very similar to the `helloTezos` contracts we wrote in
chapter one. Recalling that `CAR` selects the left-hand side of a `Pair`,
one can see that this contract returns the pair of an empty list of operations
and the parameter passed to it as the new storage.

Let's test it locally, with the `run` command we learned in chapter 1:

```
$ ~/alphanet.sh client run script container:idString.tz \
                                      on storage "foo" and \
                                      input "bar"
```

This should output

```
storage
  "bar"
emitted operations
```

indicating the storage string `"foo"` was replaced by `"bar"`.

Now let's push this contract to the Alphanet using our `alice` account from the
previous chapter:

```
$ ~/alphanet.sh client originate contract idString for alice \
                                transferring 1 from alice \
                                container:idString.tz \
                                --init '"hello"'
```

You may need to add a `--burn-cap` flag to this command.

This should output a large transaction receipt, of which the relevant lines are:


```
Originated contracts:
        KT1TDFxATmqqTcPWPJvrv7XC6EDS82ztRntf
      Storage size: 46 bytes
```

This indicates the contract address and the size of the contract and contract's
storage in bytes.  The string "hello" is 5 bytes long, and the size of our
contract (after a little minor compression) is 41. The longer our contract, or
the bigger our storage data, the more we have to pay the network.

This being the Alphanet, the fees are in play-money so we don't have to really
pay too much attention to them. Still, this notion of "pay-by-the-byte" is
important to understand: a blockchain as a way to store data scales extremely
poorly with respect to the data's size. Compared to a traditional, more
centralized, database, blockchains are outrageously expensive on a per-byte
basis, which makes intuitive sense if you consider that all on-chain data has to
get replicated across every node (or at least every baker). In general,
blockchain applications working with any data of non-trivial size should
provide an out-of-band mechanism for storing and serving data and have all
on-chain computation work with hashes of that data.

Now let's wait for the operation to get deep enough into the chain so we can
look at it on [tzscan.io](www.tzscan.io):

```
$ ~/alphanet.sh client wait for \
        op2pFJi18njQuGS3MzC39XHw2piXybTFGbAQboCdDNLTYiFTAPN to be included \
        --confirmations 5
```

After a short wait, our contract should appear in the block explorer (using the
search bar to search for our contract address), and we should be able to inspect
our contract's code,

![idString code](tzscan_idString_code.jpg)

as well as its current storage value,

![idString storage](tzscan_idString_storage.jpg)

Congratulations! We've just pushed our first contract onto the network.

## Calling a contract

We can call a contract by sending a transaction to its address, exactly like
what we did last chapter in sending tez from one account to another.

Let's try this by running the transfer command we learned last chapter, except
this time transferring to the `idString` contract instead of an account like
`alice` or `bob`.

We can see all the contracts our client knows about (and has assigned short
nicknames), with

```
$ ~/alphanet.sh client list known contracts
idString: KT1TDFxATmqqTcPWPJvrv7XC6EDS82ztRntf
bob: tz1QCUKL2YZuGLSdTesXtVHJB8zRcG6XTz8f
alice: tz1M1tuK4BM53S2niKvQDn6SbACiVk6zbjx4
...
```

This will also output the accounts from the `list known addresses` command we
used last chapter. In Tezos, accounts like `alice` are just contracts that don't
have any code, so all they can do is hold a tez balance. For convenience,
contracts that can execute code have addresses prefixed with `KT` whereas
accounts are prefixed with `tz`.

Since contracts and accounts are essentially the same thing, all we have to do
to call the Michelson code in a contract is transfer some tez to it, using the
`--arg <parameter>` flag to pass in the Michelson data we want the contract to
run on.

Our `idString` contract currently has the storage value `"hello"`, let's go
ahead and pass it the parameter `"world"`. We'll add the `--dry-run` flag to
simulate what this command will do, without actually sending anything out to the
network.

```
$ ~/alphanet.sh client transfer 0 from alice to idString \
      --arg '"world"' --dry-run

Estimated gas: 11375 units (will add 100 for safety)
Estimated storage: no bytes added
Operation: 0x ....
Operation hash: op9KQJiPETWpjMF316owi5iPzRQMRHq9nhJvSNBW6yCuCxNwxgw
Simulation result:
  Manager signed operations:
    From: tz1M1tuK4BM53S2niKvQDn6SbACiVk6zbjx4
    Fee to the baker: ꜩ0.001414
    Expected counter: 5812
    Gas limit: 11475
    Storage limit: 0 bytes
    Balance updates:
      tz1M1tuK4BM53S2niKvQDn6SbACiVk6zbjx4 ............ -ꜩ0.001414
      fees(tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU,37) ... +ꜩ0.001414
    Transaction:
      Amount: ꜩ0
      From: tz1M1tuK4BM53S2niKvQDn6SbACiVk6zbjx4
      To: KT1TDFxATmqqTcPWPJvrv7XC6EDS82ztRntf
      Parameter: "world"
      This transaction was successfully applied
      Updated storage: "world"
      Storage size: 46 bytes
      Consumed gas: 11375
```

This receipt indicates that our transaction will in fact go through. Important
points to notice in the receipt are the lines indicating fees (even a
transfer of 0 tez still has fees) and the line showing that the storage size is
still 46 bytes (because `"hello"` and `"world"` are the same length).

Now let's try a `--dry-run` of a transaction that won't work, by calling
`idString` with the parameter `42`:

```
$ ~/alphanet.sh client transfer 0 from alice to idString --arg '42' --dry-run
Node is bootstrapped, ready for injecting operations.
This simulation failed:
  Manager signed operations:
    From: tz1M1tuK4BM53S2niKvQDn6SbACiVk6zbjx4
    Fee to the baker: ꜩ0
    Expected counter: 5812
    Gas limit: 400000
    Storage limit: 60000 bytes
    Transaction:
      Amount: ꜩ0
      From: tz1M1tuK4BM53S2niKvQDn6SbACiVk6zbjx4
      To: KT1TDFxATmqqTcPWPJvrv7XC6EDS82ztRntf
      Parameter: 42
      This operation FAILED.

Invalid argument passed to contract KT1TDFxATmqqTcPWPJvrv7XC6EDS82ztRntf.
At (unshown) location 0, value 42 is invalid for type string.
At (unshown) location 0, unexpected int, only a string can be used here.
Fatal error:
  transfer simulation failed
```

As you might have expected, this fails with a type error. You have to pass a
parameter of the correct type for the transaction to be accepted by the network,
just like you have to have a balance big enough to cover the tez transferred in
the transaction.

Okay, now let's call the contract `idString` for real with the parameter
`"world"` by removing the `--dry-run` flag:

```
$ ~/alphanet.sh client transfer 0 from alice to idString --arg '"world"'
```

After a short wait we should see the updated storage value on the block
explorer:

![idString storage](tzscan_idString_storage2.jpg)

## `stringCaller`: Calling a contract from another contract

Okay, now let's do something fancy. Instead of calling our `idString` contract
from the command line, we're going to write a new contract called
`stringCaller.tz` which is going to call `idString.tz`. We will call
`stringCaller` with a string parameter, but `stringCaller` is just going to pass
that string to `idString`, which will hold it in its storage.

Here is the Michelson code for `stringCaller`:

```
# stringCaller.tz
parameter string;
storage address;
code { DUP;
       DUP;
       CDR;
       CONTRACT string;
       IF_NONE {DROP; NIL operation }
               {SWAP;
                CAR;
                DIP {PUSH mutez 0};
                TRANSFER_TOKENS;
                DIP {NIL operation;};
                CONS;
               };
       DIP { CDR };
       PAIR;
     };
```

This uses many of the Michelson op-codes you learned in chapter 1 and its
exercises (if you have not yet completed the chapter 1 exercises, please pause
here and do so now). There are two new op-codes here that handle the calling
logic:


```
  # Push the contract at an address onto the stack
  code  CONTRACT 'p
  stack addr : S => None : S
    if addr does not exist
  stack addr : S => Some addr : S
    otherwise
  type :: address : 'A  -> option (contract 'p) : 'A

  # Forge a transaction from a parameter and mutez value to a recipient contract
  code TRANSFER_TOKENS
  stack     param : tz : recipient : S => op : S
  type  ::  'p : mutez : contract 'p : 'A -> operation : 'A
```

### The CONTRACT op code and the `option` type

This introduces us to a new type: `option`. The `option` type has two data
constructors, `Some 'a`, which holds a data value of type `'a` and `None` which
is similar to a null reference in some languages. The `option` type is a common
idiom in functional programming to safely represent the possible nonexistence
or invalidity of a given piece of data.

The `IF_NONE` op code unwraps the `option`:

```
# Inspect an optional value
code IF_NONE bt bf
stack (None)   : S  =>  execute bt on S
      (Some a) : S  =>  execute bf on a : S
type :: option 'a : 'A   ->   'b : 'A
     iff   bt :: [ 'A -> 'b : 'A]
           bf :: [ 'a : 'A -> 'b : 'A]
```

This might be a little difficult to read, but essentially the idea is that if
the value on top of the stack is a `None` we run the sequence of instructions in
the first branch, dropping the `None` from the stack. If the value is a `Some 'a`
on the other hand, we put the `'a` value on top of the stack and run the second
branch's sequence of instructions. Crucially, the two branches must result in
the same stack type.

Here, our `CONTRACT 'p` op code has to safely handle the possibility that there
is no valid contract associated with the `address` (Michelson has a special
`address` type) on the stack. If this is case, the stack is updated with the
`None` data constructor. If the contract does exist, on the other hand, the
stack is updated with a typed representation (the `contract 'p` type) of the
contract at the specified address, wrapped in the `Some` constructor. This
allows us to safely interact with that contract in a variety of ways, one of
which being via the `TRANSFER_TOKENS` op code.

(Also note that the `contract` type is not introspect-able; we cannot, for
example, examine the storage type or value associated with the given contract.
All we know is the type of parameter the contract takes.)

### The `TRANSFER_TOKENS` op code and the `operation` type

The `TRANSFER_TOKENS` op code performs essentially the same function as what we
did manually at the command line previously in this chapter. It takes an
argument, mutez transfer value, and transaction recipient contract from the
stack and creates an emitted network `operation`.

You should remember the `operation` type from chapter 1, where all the contracts
you wrote had to obey the Michelson calling convention of returning the pair of
a `list operation` and an updated storage. None of the contracts you wrote
emitted any operations onto the network, so the left hand side of the return
pair was an empty list created by the `NIL` op code.

The emitted `operation` list that a contract returns is, essentially, the list
of side-effects your contract will have on the rest of the Tezos blockchain.
This could be a transaction, a contract origination, a delegation etc. The
calling convention packages all these effects into a list, and when your
contract returns, it emits this list of operations in order onto the network
(if an operation generates further new operations, they are appended to the end
of the list). In this way, Michelson contracts conveniently distinguish between
effects on their own state (the updated storage value) and effects on the global
blockchain state.

Note that the `operation` type refers only to *network* operations (such as
transactions) as opposed to *stack* operations (such as op codes like
`NIL`). This is an easy source of confusion, as the Michelson documentation (as
well as this tutorial) sometimes uses the word "operation" ambiguously for
either, depending on the context. Nevertheless, now that you are aware of the
distinction, the context should clarify which kind of operation is meant. For
example, if you read in the [Michelson
specification](http://tezos.gitlab.io/alphanet/whitedoc/michelson.html#operations-on-optional-values)
the phrase "Operations on optional values", you should infer that since optional
values are elements of the Michelson stack, that the phrase refers to *stack*
operations only.

## Typechecking `stringCaller`

Let's first type-check our `stringCaller.tz` file with `-v` flag to get a better
sense of how each operation changes the stack type. The `@parameter` and
`@storage` symbols in the following are variable annotations that the
Michelson typechecker automatically adds to help us better read the output.
We'll cover annotations in more detail in a future chapter.

```
$ ~/alphanet.sh client typecheck script container:stringCaller.tz -v
Well typed
Gas remaining: 399293 units remaining
{ parameter string ;
  storage address ;
  code { /* [ pair (string @parameter) (address @storage) ] */
         DUP
         /* [ pair (string @parameter) (address @storage)
            : pair (string @parameter) (address @storage) ] */ ;
         DUP
         /* [ pair (string @parameter) (address @storage)
            : pair (string @parameter) (address @storage)
            : pair (string @parameter) (address @storage) ] */ ;
         CDR
         /* [ @storage address : pair (string @parameter) (address @storage)
            : pair (string @parameter) (address @storage) ] */ ;
         CONTRACT
           string
         /* [ @storage.contract option (contract string)
            : pair (string @parameter) (address @storage)
            : pair (string @parameter) (address @storage) ] */ ;
         IF_NONE
           { /* [ pair (string @parameter) (address @storage)
                : pair (string @parameter) (address @storage) ] */
             DROP
             /* [ pair (string @parameter) (address @storage) ] */ ;
             NIL operation
             /* [ list operation : pair (string @parameter) (address @storage) ] */ }
           { /* [ @storage.contract.some contract string
                : pair (string @parameter) (address @storage)
                : pair (string @parameter) (address @storage) ] */
             SWAP
             /* [ pair (string @parameter) (address @storage)
                : @storage.contract.some contract string
                : pair (string @parameter) (address @storage) ] */ ;
             CAR
             /* [ @parameter string : @storage.contract.some contract string
                : pair (string @parameter) (address @storage) ] */ ;
             DIP { /* [ @storage.contract.some contract string
                      : pair (string @parameter) (address @storage) ] */
                   PUSH mutez
                        0
                   /* [ mutez : @storage.contract.some contract string
                      : pair (string @parameter) (address @storage) ] */ }
             /* [ @parameter string : mutez : @storage.contract.some contract string
                : pair (string @parameter) (address @storage) ] */ ;
             TRANSFER_TOKENS
             /* [ operation : pair (string @parameter) (address @storage) ] */ ;
             DIP { /* [ pair (string @parameter) (address @storage) ] */
                   NIL operation
                   /* [ list operation : pair (string @parameter) (address @storage) ] */ }
             /* [ operation : list operation : pair (string @parameter) (address @storage) ] */ ;
             CONS
             /* [ list operation : pair (string @parameter) (address @storage) ] */ } ;
         DIP { /* [ pair (string @parameter) (address @storage) ] */ CDR /* [ @storage address ] */ }
         /* [ list operation : @storage address ] */ ;
         PAIR
         /* [ pair (list operation) (address @storage) ] */ } }
```

This is a lot of output to read, but it'll help you follow the execution if you
try to read it operation by operation and check if the before and after stack
types match what you expect.

## Originating `stringCaller`

Now let's originate `stringCaller` the same way we did `idString`, except the
initial storage we pass to it will be the address of `idString`.

Show the address of `idString` with

```
~/alphanet.sh client show known contract idString

KT1TDFxATmqqTcPWPJvrv7XC6EDS82ztRntf
```

And now paste this into the origination command:

```
$ ~/alphanet.sh client originate contract stringCaller for alice \
    transferring 1 from alice \
    running container:stringCaller.tz \
    --init '"KT1TDFxATmqqTcPWPJvrv7XC6EDS82ztRntf"'
```

If all went well the contract should now appear on tzScan with the correct
storage:

![tzscan_stringCaller](tzscan_stringCaller_storage.jpg)

## Running `stringCaller`

Now, at last, let's call `stringCaller` with the string `"caller"`. Try
it with a `--dry-run` on your own first, and then when you're satisfied you
understand what's going on run it for real:

```
$ ~/alphanet.sh client transfer 0 from alice to stringCaller --arg '"caller"'
```

Now look at the storage for `idString` again on tzScan:

![tzscan_idString](tzscan_idString_storage3.jpg)

Voila! You have now called a Michelson contract, from another Michelson
Contract.

## Exercises

1. There is something strange about the transaction we just performed with
   stringCaller. Look at the transaction receipt for the line `Paid storage
   size difference`. Who paid the fee for the additional byte of storage?

2. Originate `stringCaller.tz` again, but this time on the `bob` account from
   the last chapter. Call this contract `stringCallerBob`. Call
   `stringCallerBob` with the parameter `"calledByBob`. Who paid the fee for the
   additional storage?

3. Modify `stringCaller.tz` so that instead of taking a `string` parameter, it
   takes a `pair string address` parameter. The address in the parameter should
   become the new transaction recipient. Call this new contract
   `stringAddrCaller`

4. Further modify `stringAddrCaller` so that the address in its parameter
   becomes its new storage value.

5. In chapter 1 you were given the semantics of the op codes
   you would need to complete each exercise. For this exercise you must practice
   looking up the op code semantics on your own. Look up the `or` type in the
   [Michelson whitedoc](http://tezos.gitlab.io/alphanet/whitedoc/michelson.html#operations-on-unions)
   and its associate op codes.

   Write a contract called `stringOrAddr` that takes an `or string address`
   parameter.  If the parameter is a `Left string`, call the contract at the
   address in its storage  with that string. If the parameter is a `Right
   address`, update the address in the storage.

